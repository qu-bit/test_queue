import datetime as dt
import time

class Producer():
    def __init__(self):
        self.current_date_time = None
    
    def task(self):
        self.current_date_time = dt.datetime.now()
        # printing to debug. One should not read STDOUT as an output.
        print(self.current_date_time)

    def job(self):
        while True:
            self.task()
            time.sleep(2)

producer = Producer()

if __name__ == '__main__':
    producer.job()